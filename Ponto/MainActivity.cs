﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;

namespace Ponto
{
	[Activity (Label = "Ponto", MainLauncher = true, Icon = "@drawable/icon", Theme = "@android:style/Theme.Holo.Light")]
	public class MainActivity : Activity
	{
		int count = 1;
		List<string> items;
		bool working = false;
		ListView _listView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			items = new List<string> ();
			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			
			button.Click += delegate {
				DateTime now = DateTime.Now.ToLocalTime();

				string line = "";
				if(!working){
					button.Text = "REGISTRAR SAÍDA";
					line = (string.Format ("Entrada: {0}", now));
					items.Add(line);
				}else{
					button.Text = "REGISTRAR ENTRADA";
					string lastItem = items.Count > 0 ? items[items.Count-1] : "";
					line = (string.Format ( "{0} / Saida: {1}",lastItem , now));
					items[items.Count-1] = line;
				}
				_listView.InvalidateViews();


				working = !working;


			};
			_listView = FindViewById<ListView> (Resource.Id.listView1);
			_listView.Adapter = new PontoListAdapter (this, items);


		}

		private class PontoListAdapter : BaseAdapter<string> {
			List<string> items;
			Activity context;
			public PontoListAdapter(Activity context, List<string> items) : base() {
				this.context = context;
				this.items = items;
			}
			public override long GetItemId(int position)
			{
				return position;
			}
			public override string this[int position] {  
				get { return items[position]; }
			}
			public override int Count {
				get { return items.Count; }
			}
			public override View GetView(int position, View convertView, ViewGroup parent)
			{
				View view = convertView; // re-use an existing view, if one is available
				if (view == null) // otherwise create a new one
					view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);

				view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = items[position];

				return view;
			}
		}
	}
}


